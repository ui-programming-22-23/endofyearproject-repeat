const canvas = document.getElementById("the_canvas")
const context = canvas.getContext("2d");


let image = new Image();
let backgroundImage = new Image();
let doorBox = new Image();
let doorBoxBottomImage = new Image();
let drawerImage = new Image();
let tableImage = new Image();
let tvImage = new Image();
let paintingImage = new Image();
let clockImage = new Image();
let teaPotImage = new Image();
let vaseImage = new Image();
let windowImage = new Image();
let bookshelfImage = new Image();

let basketImage = new Image();
let bathtubImage = new Image();
let mirrorImage = new Image();
let mrMuscleImage = new Image();
let painting2Image = new Image();
let sinkImage = new Image();
let stoolImage = new Image();
let toiletImage = new Image();
let washingMachineImage = new Image();

let fridgeImage = new Image();
let kitchenTableImage = new Image();
let kitchenSinkImage = new Image();
let ovenImage = new Image();
let phoneImage = new Image();
let radioImage = new Image();


image.src = "img/characterSpritesheet.png";
backgroundImage.src = "img/startingRoom.png";
doorBox.src = "img/doorBox.png";
doorBoxBottomImage.src = "img/doorBoxBottom.png";
drawerImage.src = "img/drawer.png";
tableImage.src = "img/table.png";
tvImage.src = "img/tv.png";
paintingImage.src = "img/painting.png";
clockImage.src = "img/clock.png";
teaPotImage.src = "img/teaPot.png";
vaseImage.src = "img/vase.png";
windowImage.src = "img/window.png";
bookshelfImage.src = "img/bookshelf.png";

basketImage.src = "img/basket.png";
bathtubImage.src = "img/bathtub.png";
mirrorImage.src = "img/mirror.png";
mrMuscleImage.src = "img/mrMuscle.png";
painting2Image.src = "img/painting2.png";
sinkImage.src = "img/sink.png";
stoolImage.src = "img/stool.png";
toiletImage.src = "img/toilet.png";
washingMachineImage.src = "img/washingMachine.png";

fridgeImage.src = "img/fridge.png";
kitchenTableImage.src = "img/kicthenTable.png";
kitchenSinkImage.src = "img/kitchenSink.png";
ovenImage.src = "img/oven.png";
phoneImage.src = "img/phone.png";
radioImage.src = "img/radio.png";

const scale = 2;
const width = 48;
const height = 64;
const scaledWidth = scale * width;
const scaledHeight = scale * height;
const walkLoop = [0, 1, 0, 2];
const frameLimit = 7;

let currentLoopIndex = 0;
let frameCount = 0;
let currentDirection = 0;
let speed = 5;

let currentRoom = 0;

let startingRoomMusic = 1;

let yesBtn = document.querySelector(".yes_btn");
yesBtn.style.display = "none";

let noBtn = document.querySelector(".no_btn");
noBtn.style.display = "none";

let continueBtn = document.querySelector(".continue_btn");
continueBtn.style.display = "none";

let restartBtn = document.querySelector(".restart_btn");
restartBtn.style.display = "none";

document.getElementById("tvForm").style.display = "none";
document.getElementById("phoneForm").style.display = "none";
document.getElementById("fridgeForm").style.display = "none";
document.getElementById("escapeDoorForm").style.display = "none";

// Set the countdown duration to 5 minutes (300 seconds)
var countdownDuration = 300;
var targetTime = new Date().getTime() + countdownDuration * 1000;

let yes = false;
let no = false;
let continueB = false;
let askingToGoKitchen = false;
let askingToGoStartingRoom = false;
let askingToGoBathroom = false;

let bookshelfMsg = false;

let tvMsg = false;
let correctTvCode = false;
let incorrectTvCode = false;

let windowMsg = false;

let hasVaseKey = false;
let vaseNoKeyMsg = false;
let vaseYesKeyMsg = false;

let hasHammer = false;
let drawerYesKeyMsg = false;
let drawerNoKeyMsg = false;

let teapotNoHammerMsg = false;
let teapotYesHammerMsg = false;

let kitchenTableMsg = false;
let hasKitchenKnife = false;

let radioNoKnifeMsg = false;
let radioYesKnifeMsg = false;
let radioKey = false;

let kitchenSinkYesKeyMsg = false;
let kitchenSinkNoKeyMsg = false;

let phoneMsg = false;
let correctNumberMsg = false;
let incorrectNumberMsg = false;

let fridgeMsg = false;
let correctFridgeCode = false;
let incorrectFridgeCode = false;

let stoolMsg = false;
let stoolKey = false;

let washingMachineYesKeyMsg = false;
let washingMachineNoKeyMsg = false;

let clockMsg = false;

let escapeDoorMsg = false;
let incorrectEscapeCodeMsg = false;
let gameWon = false;
let gameLost = false;
var timeRemaining;

let highScoreMsg = false;
let highScoreMins = 0;
let highScoreSecs = 0;

// GameObject holds positional information
// Can be used to hold other information based on requirements
function GameObject(spritesheet, x, y, width, height) {
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.mvmtDirection = "None";
}

// Default Player
let player = new GameObject(image, 200, 300, 144, 256)
let background = new GameObject(backgroundImage, 0, 0, 1980, 1080)
let doorToKitchen = new GameObject(doorBox, 100, 515, 28, 142)
let doorToStartingRoom = new GameObject(doorBox, -100, -100, 28, 142)
let doorToBathroom = new GameObject(doorBox, 1860, 515, 28, 142)
let doorToFreedom = new GameObject(doorBoxBottomImage, 900, 975, 142, 28)
let drawer = new GameObject(drawerImage, 300, 200, 92, 108)
let table = new GameObject(tableImage, 800, 550, 270, 196)
let tv = new GameObject(tvImage, 1000, 175, 144, 140)
let painting = new GameObject(paintingImage, 400, 100, 172, 98)
let clock = new GameObject(clockImage, 1750, 150, 90, 88)
let teaPot = new GameObject(teaPotImage, 865, 600, 114, 60)
let vase = new GameObject(vaseImage, 105, 860, 76, 128)
let window1 = new GameObject(windowImage, 850, 100, 136, 128)
let bookshelf = new GameObject(bookshelfImage, 1350, 100, 220, 252)

let basket = new GameObject(basketImage, -200, -200, 62, 68)
let bathtub = new GameObject(bathtubImage, -200, -200, 146, 268)
let mirror = new GameObject(mirrorImage, -200, -200, 68, 92)
let mrMuscle = new GameObject(mrMuscleImage, -200, -200, 44, 96)
let painting2 = new GameObject(painting2Image, -200, -200, 172, 98)
let sink = new GameObject(sinkImage, -200, -200, 86, 74)
let stool = new GameObject(stoolImage, -200, -200, 100, 92)
let toilet = new GameObject(toiletImage, -200, -200, 100, 164)
let washingMachine = new GameObject(washingMachineImage, -200, -200, 132, 164)

let fridge = new GameObject(fridgeImage, -200, -200, 80, 148)
let kitchenTable = new GameObject(kitchenTableImage, -200, -200, 226, 150)
let kitchenSink = new GameObject(kitchenSinkImage, -200, -200, 142, 112)
let oven = new GameObject(ovenImage, -200, -200, 88, 104)
let phone = new GameObject(phoneImage, -200, -200, 42, 72)
let radio = new GameObject(radioImage, -200, -200, 84, 116)

let backgroundMusic = new Audio("music/startingRoomMusic.mp3");

// The GamerInput is an Object that holds the Current
// GamerInput (Left, Right, Up, Down, MouseClicks)
function GamerInput(input) {
    this.action = input; // Hold the current input as a string
}

// Default GamerInput is set to None
let gamerInput = new GamerInput("None"); //No Input


function input(event) {
    // Take Input from the Player
    // console.log("Input");
    console.log(event);
    console.log("Event type: " + event.type);
    // console.log("Keycode: " + event.keyCode);

    if (event.type === "keydown") {
        switch (event.keyCode) {
            case 65: // Left Arrow
                gamerInput = new GamerInput("Left");
                break; //Left key
            case 87: // Up Arrow
                gamerInput = new GamerInput("Up");
                break; //Up key
            case 68: // Right Arrow
                gamerInput = new GamerInput("Right");
                break; //Right key
            case 83: // Down Arrow
                gamerInput = new GamerInput("Down");
                break; //Down key
            case 13: // Enter
                gamerInput = new GamerInput("Enter");
                break; //Down key
            default:
                gamerInput = new GamerInput("None"); //No Input
        }
    } else {
        gamerInput = new GamerInput("None");
    }
}


function update() {
    // console.log("Update");
    // Check Input
    if(gameLost === true)
    {
        backgroundMusic.pause();
        restartBtn.style.display = "block";
    }
    else if(gameWon === false)
    {
    if (gamerInput.action === "Up") {
        //console.log("Move Up");
        if(player.y >200)
        {
        player.y -= speed; // Move Player Up
        }
        currentDirection = 0;
    } else if (gamerInput.action === "Down") {
        //console.log("Move Down");
        if(player.y <880)
        {
        player.y += speed; // Move Player Down
        }
        currentDirection = 2;
    } else if (gamerInput.action === "Left") {
        //console.log("Move Left");
        if(player.x > 70)
        {
        player.x -= speed; // Move Player Left
        }
        currentDirection = 3;
    } else if (gamerInput.action === "Right") {
        //console.log("Move Right");
        if(player.x < 1830)
        {
        player.x += speed; // Move Player Right
        }
        currentDirection = 1;
    }  else if (gamerInput.action === "None") {
        //console.log("player no longer moving")    
    }
    

   
    backgroundMusic.play();

    
        // Update the countdown every second
        var countdownInterval = setInterval(function() {
            var now = new Date().getTime();
            timeRemaining = targetTime - now;

            if (timeRemaining <= 0)
            {
                gameLost = true;
            }
            else 
            {
                var minutes = Math.floor(timeRemaining / (60 * 1000));
                var seconds = Math.floor((timeRemaining % (60 * 1000)) / 1000);

                // Add leading zeros if necessary
                minutes = (minutes < 10) ? "0" + minutes : minutes;
                seconds = (seconds < 10) ? "0" + seconds : seconds;

                // Update the countdown display
                var countdown = minutes + ":" + seconds;
                document.getElementById("timer").innerHTML = countdown;
            }
        }, 1000); // Update every 1 second (1000 milliseconds)
    
    //checks if the player interacted with the kitchen door
    if(CollisionDetection(player.x, player.y, player.height, player.width, doorToKitchen.x, doorToKitchen.y, doorToKitchen.height, doorToKitchen.width))
    {
        // going ito the kitchen
        if (gamerInput.action === "Enter")
        {
            yesBtn.style.display = "block";
            noBtn.style.display = "block";
            askingToGoKitchen = true;
        }

        if(yes === true)
        {
            backgroundMusic.pause();
            backgroundMusic.currentTime = 0;
            backgroundMusic = new Audio("music/kitchenMusic.mp3");
            currentRoom = 1;
            changeRoom();
            yes = false;
            yesBtn.style.display = "none";
            noBtn.style.display = "none";
            askingToGoKitchen = false;
        }
        else if (no === true)
        {
            yesBtn.style.display = "none";
            noBtn.style.display = "none";
            no = false;
            askingToGoKitchen = false;
        }
    }
    else if(CollisionDetection(player.x, player.y, player.height, player.width, doorToStartingRoom.x, doorToStartingRoom.y, doorToStartingRoom.height, doorToStartingRoom.width) && currentRoom == 1)
    {
        //going into the starting room from the kicthen
        if (gamerInput.action === "Enter")
        {
            yesBtn.style.display = "block";
            noBtn.style.display = "block";
            askingToGoStartingRoom = true;
        }

        if(yes === true)
        {
            backgroundMusic.pause();
            backgroundMusic.currentTime = 0;
            backgroundMusic = new Audio("music/startingRoomMusic.mp3");
            player = new GameObject(image, 200, 500, 144, 256)
            currentRoom = 0;
            changeRoom();
            yes = false;
            yesBtn.style.display = "none";
            noBtn.style.display = "none";
            askingToGoStartingRoom = false;
        }
        else if (no === true)
        {
            yesBtn.style.display = "none";
            noBtn.style.display = "none";
            no = false;
            askingToGoStartingRoom = false;
        }
    }
    else if(CollisionDetection(player.x, player.y, player.height, player.width, doorToStartingRoom.x, doorToStartingRoom.y, doorToStartingRoom.height, doorToStartingRoom.width) && currentRoom == 2)
    {
        //going into the starting room from the bathroom
        if (gamerInput.action === "Enter")
        {
            yesBtn.style.display = "block";
            noBtn.style.display = "block";
            askingToGoStartingRoom = true;
        }

        if(yes === true)
        {
            backgroundMusic.pause();
            backgroundMusic.currentTime = 0;
            backgroundMusic = new Audio("music/startingRoomMusic.mp3");
            player = new GameObject(image, 1700, 500, 144, 256)
            currentRoom = 0;
            changeRoom();
            yes = false;
            yesBtn.style.display = "none";
            noBtn.style.display = "none";
            askingToGoStartingRoom = false;
        }
        else if (no === true)
        {
            yesBtn.style.display = "none";
            noBtn.style.display = "none";
            no = false;
            askingToGoStartingRoom = false;
        }
    }
    else if(CollisionDetection(player.x, player.y, player.height, player.width, doorToBathroom.x, doorToBathroom.y, doorToBathroom.height, doorToBathroom.width))
    {
        //going into the bathroom
        //going into the starting room from the bathroom
        if (gamerInput.action === "Enter")
        {
            yesBtn.style.display = "block";
            noBtn.style.display = "block";
            askingToGoBathroom = true;
        }

        if(yes === true)
        {
            backgroundMusic.pause();
            backgroundMusic.currentTime = 0;
            backgroundMusic = new Audio("music/bathroomMusic.mp3");
            currentRoom = 2;
            changeRoom();
            yes = false;
            yesBtn.style.display = "none";
            noBtn.style.display = "none";
            askingToGoBathroom = false;
        }
        else if (no === true)
        {
            yesBtn.style.display = "none";
            noBtn.style.display = "none";
            no = false;
            askingToGoBathroom = false;
        }
    }

    if(CollisionDetection(player.x, player.y, player.height, player.width, bookshelf.x, bookshelf.y, bookshelf.height, bookshelf.width))
    {
        if (gamerInput.action === "Enter")
        {
            continueBtn.style.display = "block";
            bookshelfMsg = true;
        }
        if(continueB === true)
        {
            continueBtn.style.display = "none";
            continueB = false;
            bookshelfMsg = false;
        }
    }

    if(CollisionDetection(player.x, player.y, player.height, player.width, window1.x, window1.y, window1.height, window1.width))
    {
        if (gamerInput.action === "Enter")
        {
            continueBtn.style.display = "block";
            windowMsg = true;
        }
        if(continueB === true)
        {
            continueBtn.style.display = "none";
            windowMsg = false;
        }
    }
    else if(CollisionDetection(player.x, player.y, player.height, player.width, tv.x, tv.y, tv.height, tv.width))
    {
        if (gamerInput.action === "Enter")
        {
            document.getElementById("tvForm").style.display = "block";
            tvMsg = true;
        }
        if(continueB === true)
        {
            continueBtn.style.display = "none";
            continueB = false;
            tvMsg = false;
            correctTvCode = false;
            incorrectTvCode = false;
            document.getElementById("tvForm").style.display = "none";
        }
    } 

    if(CollisionDetection(player.x, player.y, player.height, player.width, vase.x, vase.y, vase.height, vase.width))
    {
        if (gamerInput.action === "Enter")
        {
            continueBtn.style.display = "block";

            if(hasVaseKey === false)
            {
                vaseYesKeyMsg = true;
            }
            else
            {
                vaseNoKeyMsg = true;
            }
        }
        if(continueB === true)
        {
            continueBtn.style.display = "none";
            continueB = false;
            vaseNoKeyMsg = false;
            vaseYesKeyMsg = false;
            hasVaseKey = true;
        }
    }

    if(CollisionDetection(player.x, player.y, player.height, player.width, drawer.x, drawer.y, drawer.height, drawer.width))
    {
        if (gamerInput.action === "Enter")
        {
            continueBtn.style.display = "block";
            if(hasVaseKey === true)
            {
                drawerYesKeyMsg = true;
                hasHammer = true;
            }
            else(hasVaseKey === false)
            {
                drawerNoKeyMsg = true;
            }
        }
        if(continueB === true)
        {
            continueBtn.style.display = "none";
            continueB = false;
            drawerNoKeyMsg = false;
            drawerYesKeyMsg = false;
        }
    }

    if(CollisionDetection(player.x, player.y, player.height, player.width, teaPot.x, teaPot.y, teaPot.height, teaPot.width))
    {
        if (gamerInput.action === "Enter")
        {
            continueBtn.style.display = "block";
            if(hasHammer === true)
            {
                teapotYesHammerMsg = true;
            }
            else(hasHammer === false)
            {
                teapotNoHammerMsg = true;
            }
        }
        if(continueB === true)
        {
            continueBtn.style.display = "none";
            continueB = false;
            teapotNoHammerMsg = false;
            teapotYesHammerMsg = false;
        }
    }

    if(CollisionDetection(player.x, player.y, player.height, player.width, kitchenTable.x, kitchenTable.y, kitchenTable.height, kitchenTable.width))
    {
        if (gamerInput.action === "Enter")
        {
            continueBtn.style.display = "block";
            kitchenTableMsg = true;
        }
        if(continueB === true)
        {
            continueBtn.style.display = "none";
            continueB = false;
            kitchenTableMsg = false;
            hasKitchenKnife = true;
        }
    }

    if(CollisionDetection(player.x, player.y, player.height, player.width, radio.x, radio.y, radio.height, radio.width))
    {
        if (gamerInput.action === "Enter")
        {
            continueBtn.style.display = "block";
            if(hasKitchenKnife === true)
            {
                radioYesKnifeMsg = true;
                radioKey = true;
            }
            else
            {
                radioNoKnifeMsg = true;
            }
        }
        if(continueB === true)
        {
            continueBtn.style.display = "none";
            continueB = false;
            radioYesKnifeMsg = false;
            radioNoKnifeMsg = false;
        }
    }

    if(CollisionDetection(player.x, player.y, player.height, player.width, kitchenSink.x, kitchenSink.y, kitchenSink.height, kitchenSink.width))
    {
        if (gamerInput.action === "Enter")
        {
            continueBtn.style.display = "block";
            if(radioKey === true)
            {
                kitchenSinkYesKeyMsg = true;
            }
            else
            {
                kitchenSinkNoKeyMsg = true;
            }
        }
        if(continueB === true)
        {
            continueBtn.style.display = "none";
            continueB = false;
            kitchenSinkYesKeyMsg = false;
            kitchenSinkNoKeyMsg = false;
        }
    }

    if(CollisionDetection(player.x, player.y, player.height, player.width, phone.x, phone.y, phone.height, phone.width))
    {
        if (gamerInput.action === "Enter")
        {
            document.getElementById("phoneForm").style.display = "block";
            phoneMsg = true;
        }
        if(continueB === true)
        {
            continueBtn.style.display = "none";
            continueB = false;
            phoneMsg = false;
            correctNumberMsg = false;
            incorrectNumberMsg = false;
            document.getElementById("phoneForm").style.display = "none";
        }
    }

    if(CollisionDetection(player.x, player.y, player.height, player.width, fridge.x, fridge.y, fridge.height, fridge.width))
    {
        if (gamerInput.action === "Enter")
        {
            document.getElementById("fridgeForm").style.display = "block";
            fridgeMsg = true;
        }
        if(continueB === true)
        {
            continueBtn.style.display = "none";
            continueB = false;
            fridgeMsg = false;
            correctFridgeCode = false;
            incorrectFridgeCode = false;
            document.getElementById("fridgeForm").style.display = "none";
        }
    }

    if(CollisionDetection(player.x, player.y, player.height, player.width, stool.x, stool.y, stool.height, stool.width))
    {
        if (gamerInput.action === "Enter")
        {
            continueBtn.style.display = "block";
            stoolMsg = true;
        }
        if(continueB === true)
        {
            continueBtn.style.display = "none";
            continueB = false;
            stoolMsg = false;
            stoolKey = true;
        }
    }

    if(CollisionDetection(player.x, player.y, player.height, player.width, washingMachine.x, washingMachine.y, washingMachine.height, washingMachine.width))
    {
        if (gamerInput.action === "Enter")
        {
            continueBtn.style.display = "block";
            if(stoolKey === true)
            {
                washingMachineYesKeyMsg = true;
            }
            else
            {
                washingMachineNoKeyMsg = true;
            }
        }
        if(continueB === true)
        {
            continueBtn.style.display = "none";
            continueB = false;
            washingMachineYesKeyMsg = false;
            washingMachineNoKeyMsg = false;
        }
    }

    if(CollisionDetection(player.x, player.y, player.height, player.width, clock.x, clock.y, clock.height, clock.width))
    {
        if (gamerInput.action === "Enter")
        {
            continueBtn.style.display = "block";
            clockMsg = true;
        }
        if(continueB === true)
        {
            continueBtn.style.display = "none";
            continueB = false;
            clockMsg = false;
        }
    }

    if(CollisionDetection(player.x, player.y, player.height, player.width, doorToFreedom.x, doorToFreedom.y, doorToFreedom.height, doorToFreedom.width))
    {
        if (gamerInput.action === "Enter")
        {
            document.getElementById("escapeDoorForm").style.display = "block";
            escapeDoorMsg = true;
        }
        if(continueB === true)
        {
            continueBtn.style.display = "none";
            continueB = false;
            escapeDoorMsg = false;
            incorrectEscapeCodeMsg = false;
            document.getElementById("escapeDoorForm").style.display = "none";
        }
    }
    }
    else
    {

        backgroundMusic.pause();
        restartBtn.style.display = "block";
        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("You Win!", 600, 450); 
    }
    
}

function drawFrame(image, frameX, frameY, canvasX, canvasY) {
    context.drawImage(image,
                  frameX * width, frameY * height, width, height,
                  canvasX, canvasY, scaledWidth, scaledHeight);
}

function CollisionDetection(obj1x, obj1y, obj1height, obj1width, obj2x, obj2y, obj2height, obj2width,)
{

    // Calculate the edges of the rectangles
    const obj1Left = obj1x;
    const obj1Right = obj1x + obj1width;
    const obj1Top = obj1y;
    const obj1Bottom = obj1y + obj1height;

    const obj2Left = obj2x;
    const obj2Right = obj2x + obj2width;
    const obj2Top = obj2y;
    const obj2Bottom = obj2y + obj2height;

    // Check for collision
    if (
        obj1Left < obj2Right &&
        obj1Right > obj2Left &&
        obj1Top < obj2Bottom &&
        obj1Bottom > obj2Top
    ) {
        return true; // Objects are touching
    } else {
        return false; // Objects are not touching
    }

}

function changeRoom()
{
    if (currentRoom === 0)
    {
        doorToKitchen.x = 100;
        doorToKitchen.y = 515;

        doorToStartingRoom.x = -100;
        doorToStartingRoom.y = -100;

        doorToBathroom.x = 1860;
        doorToBathroom.y = 515

        doorToFreedom.x = 900;
        doorToFreedom.y = 975

        backgroundImage.src = "img/startingRoom.png";

        drawer = new GameObject(drawerImage, 300, 200, 92, 108)
        table = new GameObject(tableImage, 800, 550, 270, 196)
        tv = new GameObject(tvImage, 1000, 175, 144, 140)
        painting = new GameObject(paintingImage, 400, 100, 172, 98)
        clock = new GameObject(clockImage, 1750, 150, 90, 88)
        teaPot = new GameObject(teaPotImage, 865, 600, 114, 60)
        vase = new GameObject(vaseImage, 105, 860, 76, 128)
        window1 = new GameObject(windowImage, 850, 100, 136, 128)
        bookshelf = new GameObject(bookshelfImage, 1350, 100, 220, 252)

        basket = new GameObject(basketImage, -200, -200, 62, 68)
        bathtub = new GameObject(bathtubImage, -200, -200, 146, 268)
        mirror = new GameObject(mirrorImage, -200, -200, 68, 92)
        mrMuscle = new GameObject(mrMuscleImage, -200, -200, 44, 96)
        painting2 = new GameObject(painting2Image, -200, -200, 172, 98)
        sink = new GameObject(sinkImage, -200, -200, 86, 74)
        stool = new GameObject(stoolImage, -200, -200, 100, 92)
        toilet = new GameObject(toiletImage, -200, -200, 100, 164)
        washingMachine = new GameObject(washingMachineImage, -200, -200, 132, 164)

        fridge = new GameObject(fridgeImage, -200, -200, 80, 148)
        kitchenTable = new GameObject(kitchenTableImage, -200, -200, 226, 150)
        kitchenSink = new GameObject(kitchenSinkImage, -200, -200, 142, 112)
        oven = new GameObject(ovenImage, -200, -200, 88, 104)
        phone = new GameObject(phoneImage, -200, -200, 42, 72)
        radio = new GameObject(radioImage, -200, -200, 84, 116)
    }
    else if (currentRoom === 1)
    {
        player = new GameObject(image, 1700, 500, 144, 256)
        doorToStartingRoom.x = 1860;
        doorToStartingRoom.y = 515;

        doorToKitchen.x = -100;
        doorToKitchen.y = -100;

        doorToFreedom.x = -200;
        doorToFreedom.y = -200
        backgroundImage.src = "img/Kitchen.png";

        drawer = new GameObject(drawerImage, -200, -200, 92, 108)
        table = new GameObject(tableImage, -200, -200, 270, 196)
        tv = new GameObject(tvImage, -200, -200, 144, 140)
        painting = new GameObject(paintingImage, -200, -200, 172, 98)
        clock = new GameObject(clockImage, -200, -200, 90, 88)
        teaPot = new GameObject(teaPotImage, -200, -200, 114, 60)
        vase = new GameObject(vaseImage, -200, -200, 76, 128)
        window1 = new GameObject(windowImage, -200, -200, 136, 128)
        bookshelf = new GameObject(bookshelfImage, -250, -250, 220, 252)

        fridge = new GameObject(fridgeImage, 400, 150, 80, 148)
        kitchenTable = new GameObject(kitchenTableImage, 400, 500, 226, 150)
        kitchenSink = new GameObject(kitchenSinkImage, 900, 185, 142, 112)
        oven = new GameObject(ovenImage, 650, 200, 88, 104)
        phone = new GameObject(phoneImage, 1200, 180, 42, 72)
        radio = new GameObject(radioImage, 150, 190, 84, 116)
    }
    else if (currentRoom === 2)
    {
        player = new GameObject(image, 200, 500, 144, 256)
        doorToKitchen.x = -100;
        doorToKitchen.y = -100;

        doorToStartingRoom.x = 100;
        doorToStartingRoom.y = 515;

        doorToBathroom.x = -100;
        doorToBathroom.y = -100;

        doorToFreedom.x = -200;
        doorToFreedom.y = -200;
        backgroundImage.src = "img/bathroom.png";

        drawer = new GameObject(drawerImage, -200, -200, 92, 108)
        table = new GameObject(tableImage, -200, -200, 270, 196)
        tv = new GameObject(tvImage, -200, -200, 144, 140)
        painting = new GameObject(paintingImage, -200, -200, 172, 98)
        clock = new GameObject(clockImage, -200, -200, 90, 88)
        teaPot = new GameObject(teaPotImage, -200, -200, 114, 60)
        vase = new GameObject(vaseImage, -200, -200, 76, 128)
        window1 = new GameObject(windowImage, -200, -200, 136, 128)
        bookshelf = new GameObject(bookshelfImage, -250, -250, 220, 252)

        basket = new GameObject(basketImage, 950, 175, 62, 68)
        bathtub = new GameObject(bathtubImage, 1745, 725, 146, 268)
        mirror = new GameObject(mirrorImage, 1385, 100, 68, 92)
        mrMuscle = new GameObject(mrMuscleImage, 840, 606, 44, 96)
        painting2 = new GameObject(painting2Image, 550, 100, 172, 98)
        sink = new GameObject(sinkImage, 1175, 200, 86, 74)
        stool = new GameObject(stoolImage, 930, 225, 100, 92)
        toilet = new GameObject(toiletImage, 1750, 200, 100, 164)
        washingMachine = new GameObject(washingMachineImage, 800, 700, 132, 164)
    }
}


function animate() {
    if (gamerInput.action != "None"){
        frameCount++;
        if (frameCount >= frameLimit) {
            frameCount = 0;
            currentLoopIndex++;
            if (currentLoopIndex >= walkLoop.length) {
                currentLoopIndex = 0;
            }
        }      
    }
    else{
        currentLoopIndex = 1;
    }
    drawFrame(player.spritesheet, walkLoop[currentLoopIndex], currentDirection, player.x, player.y);
}

function draw() {
    // Clear Canvas
    context.clearRect(0,0, canvas.width, canvas.height);

    if(gameWon === false)
    {
    context.drawImage(background.spritesheet, 
        background.x,
        background.y,
        background.width,
        background.height);
    context.drawImage(drawer.spritesheet, 
        drawer.x,
        drawer.y,
        drawer.width,
        drawer.height);
    context.drawImage(table.spritesheet, 
        table.x,
        table.y,
        table.width,
        table.height);
    context.drawImage(tv.spritesheet, 
        tv.x,
        tv.y,
        tv.width,
        tv.height);
    context.drawImage(painting.spritesheet, 
        painting.x,
        painting.y,
        painting.width,
        painting.height);
    context.drawImage(teaPot.spritesheet, 
        teaPot.x,
        teaPot.y,
        teaPot.width,
        teaPot.height);
    context.drawImage(vase.spritesheet, 
        vase.x,
        vase.y,
        vase.width,
        vase.height);
    context.drawImage(window1.spritesheet, 
        window1.x,
        window1.y,
        window1.width,
        window1.height);
    context.drawImage(clock.spritesheet, 
        clock.x,
        clock.y,
        clock.width,
        clock.height);
    context.drawImage(bookshelf.spritesheet, 
        bookshelf.x,
        bookshelf.y,
        bookshelf.width,
        bookshelf.height);


    context.drawImage(stool.spritesheet, 
        stool.x,
        stool.y,
        stool.width,
        stool.height);
    context.drawImage(bathtub.spritesheet, 
        bathtub.x,
        bathtub.y,
        bathtub.width,
        bathtub.height);
    context.drawImage(mirror.spritesheet, 
        mirror.x,
        mirror.y,
        mirror.width,
        mirror.height);
    context.drawImage(mrMuscle.spritesheet, 
        mrMuscle.x,
        mrMuscle.y,
        mrMuscle.width,
        mrMuscle.height);
    context.drawImage(basket.spritesheet, 
        basket.x,
        basket.y,
        basket.width,
        basket.height);
    context.drawImage(toilet.spritesheet, 
        toilet.x,
        toilet.y,
        toilet.width,
        toilet.height);
    context.drawImage(washingMachine.spritesheet, 
        washingMachine.x,
        washingMachine.y,
        washingMachine.width,
        washingMachine.height);
    context.drawImage(painting2.spritesheet, 
        painting2.x,
        painting2.y,
        painting2.width,
        painting2.height);
    context.drawImage(sink.spritesheet, 
        sink.x,
        sink.y,
        sink.width,
        sink.height);

    context.drawImage(fridge.spritesheet, 
        fridge.x,
        fridge.y,
        fridge.width,
        fridge.height);
    context.drawImage(kitchenTable.spritesheet, 
        kitchenTable.x,
        kitchenTable.y,
        kitchenTable.width,
        kitchenTable.height);
    context.drawImage(kitchenSink.spritesheet, 
        kitchenSink.x,
        kitchenSink.y,
        kitchenSink.width,
        kitchenSink.height);
    context.drawImage(oven.spritesheet, 
        oven.x,
        oven.y,
        oven.width,
        oven.height);
    context.drawImage(radio.spritesheet, 
        radio.x,
        radio.y,
        radio.width,
        radio.height);
    context.drawImage(phone.spritesheet, 
        phone.x,
        phone.y,
        phone.width,
        phone.height);


    animate();

    if(askingToGoKitchen === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("Do you want to go to the kitchen?", 540, 450);
    }
    else if (askingToGoBathroom === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("Do you want to go to the bathroom?", 540, 450);
    }
    else if (askingToGoStartingRoom === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("Do you want to go to the starting room?", 540, 450); 
    }

    if(bookshelfMsg === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "30px Arial";
        context.fillText("Among the books, you find a diary that", 600, 450); 
        context.fillText("belonged to the previous occupant of the house.", 600, 490); 
        context.fillText("In the diary it mentions that the occupants favourite", 600, 530); 
        context.fillText("TV show was The Walking Dead on channel 6", 600, 570); 
    }

    if(tvMsg === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("Input TV channel number", 600, 450); 
    }

    if(correctTvCode === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("Season 10 Episode 22: Here's Negan", 600, 450); 
    }

    if(incorrectTvCode === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("There's nothing on this channel", 600, 450); 
    }

    if(windowMsg === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("Amidst the thick forest fog, a chilling encounter:", 450, 450); 
        context.fillText("a shadowy figure lurking in the obsidian night", 450, 500); 
    }

    if(vaseYesKeyMsg === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("Inside the vase you find a key!", 450, 450); 
    }

    if(vaseNoKeyMsg === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("Just a regular vase", 450, 450); 
    }

    if(drawerNoKeyMsg === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("The drawer is locked, it requires a key", 450, 450); 
    }

    if(drawerYesKeyMsg === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("You unlock the drawer with the key.", 550, 450); 
        context.fillText("You find a hammer, you pick it up.", 550, 510); 
    }
    
    if(teapotNoHammerMsg === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("Just a strong sturdy teaPot.", 550, 450); 
    }

    if(teapotYesHammerMsg === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("You smash the teaPot. Inside there is a note.", 550, 450); 
        context.fillText("The note reads 'No2: 9'.", 550, 510); 
    }

    if(kitchenTableMsg === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("You pick up the kicthen knife from the table", 550, 450); 
    }

    if(radioNoKnifeMsg === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("'Quarter To Three' by Gary Bonds is playing", 550, 450); 
    }

    if(radioYesKnifeMsg === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("'Quarter To Three' by Gary Bonds is playing", 550, 450); 
        context.fillText("You find a secret compartment in the back of the radio.", 550, 510);
        context.fillText("You pry it open and find a Key!.", 550, 570);
    }

    if(kitchenSinkNoKeyMsg === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("The storage compartment on the bottom is locked", 550, 450); 
    }

    if(kitchenSinkYesKeyMsg === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "30px Arial";
        context.fillText("You unlocked the bottom of the compartment.", 550, 450); 
        context.fillText("Theres a piece of paper with phone numbers on it.", 550, 470); 
        context.font = "20px Arial";
        context.fillText("Jack: 0894017073", 550, 500); 
        context.fillText("Peter: 089145698", 550, 530); 
        context.fillText("Jake: 085144789", 550, 550); 
        context.fillText("Thomas; 089477565", 550, 570); 
        context.fillText("Daniel: 085472301", 550, 590); 
        context.fillText("Gordan: 085621489", 550, 610); 
        context.fillText("Negan: 089474556", 550, 630); 
        context.fillText("James: 089445688", 550, 650); 
        context.fillText("Connor: 089741256", 550, 670); 
        context.fillText("Alex: 089998471", 550, 690); 
        context.fillText("Fred: 085684756", 550, 710); 

    }

    if(phoneMsg === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500); 
    }

    if(correctNumberMsg === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("A voice whispers: '983'", 550, 450); 
    }

    if(incorrectNumberMsg === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("No one picked up.....", 550, 450); 
    }

    if(fridgeMsg === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("The fridge is locked with a combinatiojn lock", 550, 450); 
    }

    if(correctFridgeCode === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("You unlock the fridge, inside you see", 550, 450); 
        context.fillText("a note that reads, 'No1: 7'", 550, 610); 
    }

    if(incorrectFridgeCode === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("Wrong combination code", 550, 450); 
    }

    if(stoolMsg === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("On the bottom of the basket, you find a key", 500, 450); 
    }

    if(washingMachineNoKeyMsg === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("Washing machine is locked", 500, 450); 
    }

    if(washingMachineYesKeyMsg === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("You unlock the washing machine with the key", 480, 450); 
        context.fillText("from the basket.", 480, 510);
        context.fillText("You find a note that reads: No3: Clock", 480, 570); 
    }

    if(clockMsg === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("The time is stuck on 1 o'clock.", 480, 450); 
    }

    if(escapeDoorMsg === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("The main door requires a code:", 480, 450); 
    }

    if(incorrectEscapeCodeMsg === true)
    {
        context.fillStyle = "white";
        context.fillRect(380, 325, 1200, 500);

        context.fillStyle = "black";
        context.font = "50px Arial";
        context.fillText("Wrong code", 480, 450); 
    }
    }
    else if (gameWon === true)
    {
        context.fillStyle = "black";
        context.font = "200px Arial";
        context.fillText("You Win!", 600, 450); 
    }
    if (gameLost === true)
    {
        context.fillStyle = "white";
        context.fillRect(0, 0, 1920, 1080);
        
        context.fillStyle = "black";
        context.font = "200px Arial";
        context.fillText("You Lose!", 600, 450); 
    }
    

    
        
}

function yesPressed()
{
    yes = true;
}

function noPressed()
{
    no = true;
}

function continuePressed()
{
    continueB = true;
}

function tvChannelOn()
{
    var channel = document.getElementById("tv").value;

    if (channel === "6")
    {
        document.getElementById("tvForm").style.display = "none";
        tvMsg = false;
        correctTvCode = true;
    }
    else
    {
        document.getElementById("tvForm").style.display = "none";
        tvMsg = false;
        incorrectTvCode = true;
    }
    continueBtn.style.display = "block";
}

function phoneOn()
{
    var channel = document.getElementById("phone").value;
    if (channel === "089474556")
    {
        document.getElementById("phoneForm").style.display = "none";
        phoneMsg = false;
        correctNumberMsg = true;
    }
    else
    {
        document.getElementById("phoneForm").style.display = "none";
        phoneMsg = false;
        incorrectNumberMsg = true;
    }
    continueBtn.style.display = "block";
}

function fridgeOn()
{
    var channel = document.getElementById("fridge").value;
    if (channel === "983")
    {
        document.getElementById("fridgeForm").style.display = "none";
        fridgeMsg = false;
        correctFridgeCode = true;
    }
    else
    {
        document.getElementById("fridgeForm").style.display = "none";
        fridgeMsg = false;
        incorrectFridgeCode = true;
    }
    continueBtn.style.display = "block";
}

function escapeDoor()
{
    var channel = document.getElementById("escape").value;
    if (channel === "791")
    {
        document.getElementById("escapeDoorForm").style.display = "none";
        escapeDoorMsg = false;
        gameWon = true;
        continueB = false;
    }
    else
    {
        document.getElementById("escapeDoorForm").style.display = "none";
        incorrectEscapeCodeMsg = true;
        escapeDoorMsg = false;
        continueBtn.style.display = "block";
    }
}

function restart()
{
    gameWon = false;
    gameLost = false;

    player = new GameObject(image, 200, 300, 144, 256)
    background = new GameObject(backgroundImage, 0, 0, 1980, 1080)
    doorToKitchen = new GameObject(doorBox, 100, 515, 28, 142)
    doorToStartingRoom = new GameObject(doorBox, -100, -100, 28, 142)
    doorToBathroom = new GameObject(doorBox, 1860, 515, 28, 142)
    doorToFreedom = new GameObject(doorBoxBottomImage, 900, 975, 142, 28)
    drawer = new GameObject(drawerImage, 300, 200, 92, 108)
    table = new GameObject(tableImage, 800, 550, 270, 196)
    tv = new GameObject(tvImage, 1000, 175, 144, 140)
    painting = new GameObject(paintingImage, 400, 100, 172, 98)
    clock = new GameObject(clockImage, 1750, 150, 90, 88)
    teaPot = new GameObject(teaPotImage, 865, 600, 114, 60)
    vase = new GameObject(vaseImage, 105, 860, 76, 128)
    window1 = new GameObject(windowImage, 850, 100, 136, 128)
    bookshelf = new GameObject(bookshelfImage, 1350, 100, 220, 252)

    basket = new GameObject(basketImage, -200, -200, 62, 68)
    bathtub = new GameObject(bathtubImage, -200, -200, 146, 268)
    mirror = new GameObject(mirrorImage, -200, -200, 68, 92)
    mrMuscle = new GameObject(mrMuscleImage, -200, -200, 44, 96)
    painting2 = new GameObject(painting2Image, -200, -200, 172, 98)
    sink = new GameObject(sinkImage, -200, -200, 86, 74)
    stool = new GameObject(stoolImage, -200, -200, 100, 92)
    toilet = new GameObject(toiletImage, -200, -200, 100, 164)
    washingMachine = new GameObject(washingMachineImage, -200, -200, 132, 164)

    fridge = new GameObject(fridgeImage, -200, -200, 80, 148)
    kitchenTable = new GameObject(kitchenTableImage, -200, -200, 226, 150)
    kitchenSink = new GameObject(kitchenSinkImage, -200, -200, 142, 112)
    oven = new GameObject(ovenImage, -200, -200, 88, 104)
    phone = new GameObject(phoneImage, -200, -200, 42, 72)
    radio = new GameObject(radioImage, -200, -200, 84, 116)

    backgroundMusic.pause();
    backgroundMusic.currentTime = 0;
    backgroundMusic = new Audio("music/startingRoomMusic.mp3");

    stoolKey = false;
    radioKey = false;
    hasKitchenKnife = false;
    hasVaseKey = false;
    hasHammer = false;

    countdownDuration = 300;
    targetTime = new Date().getTime() + countdownDuration * 1000;

    backgroundImage.src = "img/startingRoom.png";

    restartBtn.style.display = "none";

    highScoreMsg = false;
}

function gameloop() {
    update();
    draw();
    window.requestAnimationFrame(gameloop);
}

// Handle Active Browser Tag Animation
window.requestAnimationFrame(gameloop);


window.addEventListener('keydown', input);
// disable the second event listener if you want continuous movement
window.addEventListener('keyup', input);